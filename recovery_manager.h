/**
 * C++ record manager implementation (PODC 2015) by Trevor Brown.
 * 
 * Copyright (C) 2015 Trevor Brown
 *
 */

#ifndef RECOVERY_MANAGER_H
#define	RECOVERY_MANAGER_H

#include <setjmp.h>

#ifndef VERBOSE
    #define VERBOSE if(0)
#endif

#ifndef TRACE
    #define TRACE if(0)
#endif

#ifndef AJDBG
    #define AJDBG if(0)
#endif

#include <cassert>
#include <csignal>
// #include "globals.h"
#include "plaf.h"

// for crash recovery
/*PAD;*/
static pthread_key_t pthreadkey;
static struct sigaction ___act;
static void *___singleton = NULL;
/*PAD;*/
// extern pthread_key_t pthreadkey;
// extern struct sigaction ___act;
// extern void *___singleton;

static pthread_t registeredThreads[MAX_THREADS_POW2];
// static void *errnoThreads[MAX_THREADS_POW2];
PAD;
static sigjmp_buf *setjmpbuffers;
PAD;
// volatile thread_local bool restartable = 0; 
thread_local int restartable = 0; // removing volatile for FAA and FE version. I may need to use FAA/CAS in crashhandler as a result.
PAD;
// extern pthread_t registeredThreads[MAX_THREADS_POW2];
// extern void *errnoThreads[MAX_THREADS_POW2];

#define JUMPBUF_PAD 8
#define MAX_THREAD_ADDR 10000

#define CHECKPOINT_TR(tid, recmgr) \
    int ____jump_ret_val; \
    while( ((recmgr)->needsSetJmp()) && (____jump_ret_val = sigsetjmp(setjmpbuffers[(tid)*JUMPBUF_PAD], 0/*not saving sigmask thats costly. So after end of sighandler call explicit unblocking of signal is needed.*/)) ) { \
        (recmgr)->recoveryMgr->unblockCrashRecoverySignal(); \
    } 
    
void trcrashhandler(int signum, siginfo_t *info, void *uctx) 
{
    //USER Warning: printf cout in here with longjmp causes hang
    int tid = (int) ((long) pthread_getspecific(pthreadkey));    
    if(!restartable) {
        return;
    }

    assert ("restartable" && restartable == 1);
    restartable = 0; //if I am CASing restartable in startOP then needs to set 0 here. As CAS compares with old val 0. Not needed if not using CAS.  
    assert ("restartable" && restartable == 0);
    siglongjmp(setjmpbuffers[tid*JUMPBUF_PAD], 1); 
}

class RecoveryMgr {
public:
    PAD;
    const int NUM_PROCESSES;
    const int neutralizeSignal;
    PAD;
    
    // inline int getTidInefficient(const pthread_t me) {
    //     int tid = -1;
    //     for (int i=0;i<NUM_PROCESSES;++i) {
    //         if (pthread_equal(registeredThreads[i], me)) {
    //             tid = i;
    //         }
    //     }
    //     // fail to find my tid -- should be impossible
    //     if (tid == -1) {
    //         COUTATOMIC("THIS SHOULD NEVER HAPPEN"<<std::endl);
    //         assert(false);
    //         exit(-1);
    //     }
    //     return tid;
    // }
    // inline int getTidInefficientErrno() {
    //     int tid = -1;
    //     for (int i=0;i<NUM_PROCESSES;++i) {
    //         // here, we use the fact that errno is defined to be a thread local variable
    //         if (&errno == errnoThreads[i]) {
    //             tid = i;
    //         }
    //     }
    //     // fail to find my tid -- should be impossible
    //     if (tid == -1) {
    //         COUTATOMIC("THIS SHOULD NEVER HAPPEN"<<std::endl);
    //         assert(false);
    //         exit(-1);
    //     }
    //     return tid;
    // }


    inline int getTid_pthread_getspecific() {
        void * result = pthread_getspecific(pthreadkey);
        if (!result) {
            assert(false);
            // COUTATOMIC("ERROR: failed to get thread id using pthread_getspecific"<<std::endl);
            exit(-1);
        }
        return (int) ((long) result);
    }
    inline pthread_t getPthread(const int tid) {
        // TRACE AJDBG COUTATOMICTID("getPthread:: pthreadself:"<<pthread_self()<<" registeredtid:"<<registeredThreads[tid]<<std::endl); //@J
        return registeredThreads[tid];
    }
    
    void initThread(const int tid) {
        if (1/*MasterRecordMgr::supportsCrashRecovery() || MasterRecordMgr::needsSetJmp()*/) {
            // create mapping between tid and pthread_self for the signal handler
            // and for any thread that neutralizes another
            registeredThreads[tid] = pthread_self();
            
            // DEBUG COUTATOMICTID("RECVRY::initThread pthreadself:"<<pthread_self()<<" registeredtid:"<<registeredThreads[tid]<<std::endl); //@J
            // here, we use the fact that errno is defined to be a thread local variable
            // errnoThreads[tid] = &errno;
            if (pthread_setspecific(pthreadkey, (void*) (long) tid)) {
                COUTATOMIC("ERROR: failure of pthread_setspecific for tid="<<tid<<std::endl);
            }
            const long __readtid = (long) ((int *) pthread_getspecific(pthreadkey));
            // VERBOSE DEBUG COUTATOMICTID("did pthread_setspecific, pthread_getspecific of "<<__readtid<<std::endl);
            assert(__readtid == tid);
        }
    }
    void deinitThread(const int tid) {
        // assert (pthread_self() == registeredThreads[tid] && "LOL, tid's mismatch is deadly for TR");
    }
    
    void unblockCrashRecoverySignal() {
        if (1/*MasterRecordMgr::supportsCrashRecovery() || MasterRecordMgr::needsSetJmp()*/) {
            __sync_synchronize();
            sigset_t oldset;
            sigemptyset(&oldset);
            sigaddset(&oldset, neutralizeSignal);
            if (pthread_sigmask(SIG_UNBLOCK, &oldset, NULL)) {
                // VERBOSE COUTATOMIC("ERROR UNBLOCKING SIGNAL"<<std::endl);
                exit(-1);
            }
        }
    }
    
    RecoveryMgr(const int numProcesses, const int _neutralizeSignal)
            : NUM_PROCESSES(numProcesses) , neutralizeSignal(_neutralizeSignal){
        
        // if (MasterRecordMgr::supportsCrashRecovery() || MasterRecordMgr::needsSetJmp()) 
        {
            // COUTATOMIC("RecoveryMgr numProcesses = "<<numProcesses << std::endl);
            // std::cout<<std::flush;
            setjmpbuffers = new sigjmp_buf[numProcesses*JUMPBUF_PAD];
            pthread_key_create(&pthreadkey, NULL);
        
            // set up crash recovery signal handling for this process
            memset(&___act, 0, sizeof(___act));
            // if(MasterRecordMgr::supportsCrashRecovery())
            //     ___act.sa_sigaction = crashhandler<MasterRecordMgr>; // specify signal handler
            // else if (MasterRecordMgr::needsSetJmp())
            {
                ___act.sa_sigaction = trcrashhandler; // specify signal handler
            }
            // else
            //     COUTATOMIC("ERROR: crash handler not specified?"<<std::endl);
            
            ___act.sa_flags = SA_RESTART | SA_SIGINFO; // restart any interrupted sys calls instead of silently failing
            sigfillset(&___act.sa_mask);               // block signals during handler
            if (sigaction(_neutralizeSignal, &___act, NULL)) {
                // COUTATOMIC("ERROR: could not register signal handler for signal "<<_neutralizeSignal<<std::endl);
                assert(false);
                exit(-1);
            } 
            // else
            // {
            //     VERBOSE COUTATOMIC("registered signal "<<_neutralizeSignal<<" for crash recovery"<<std::endl);
            // }
            
            // set up shared pointer to this class instance for the signal handler
            // ___singleton = (void *) masterRecordMgr;
        }
    }
    ~RecoveryMgr() {
        // if (MasterRecordMgr::supportsCrashRecovery() || MasterRecordMgr::needsSetJmp() ) 
        {
            delete[] setjmpbuffers;
        }
    }
};

#endif	/* RECOVERY_MANAGER_H */

