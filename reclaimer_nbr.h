/* RECLAIMER NBR.h
 *   by trbot and @J
 *
 * Created:
 *   9/1/2020, 8:57:21 PM (Actually created around Dec 2019)
 * Last edited:
 *   9/1/2020, 8:59:37 PM
 *
 * Description:
 *   safe memory reclamation algorithm based on signal based neutralization.
 * Instructions to users:
 *   A data structure operation should be viewed in two phases: 1) search or traversal phase where a thread discovers new pointers enroute its target location over a data structure. Think of traversal over a lazylist. No write operations occur during this phase. 2) update phase where a thread having discovered the target location (for example pred and curr in a lazylist) prepares to write in data structure, for ex using CAS or FAA.
 *   At the beginning of search phase call startOp(). When search phase ends protect the discovered pointers (for ex, in case of the lazylist discovered pointers are pred and curr) using saveForWritePhase() and upgradeToWritePhase(). The upgradeToWritePhase() separates the two aforementioned phases of the ds operation. Ensure that post invocation of upgradeToWritePhase() a unprotected record is not discovered. This is required to maintain safety property of the NBR algorithm.)
 *  A DS operation shall not invoke system calls in search phase. Ex, free or mallocs in search phase (or outside write phase) would cause undefined behaviour as a thread in mid of these call could be siglongjmped leading to undefined behaviour.
 *   Whenever the DS operation returns user shall call endOp to mark the end of write phase.
 * DEPENDENCY:
 *   This file requires signal setup related code: signal registration, and signal handler definition, sigset and siglongjmp buffer defined in recovery_manager.h.
 * KNOWN ISSUE:
 * Still Donot know why skipping to collect own HPS causes validation fail in Gtree.
 *  - Should check if that happens with List tree as well or it occurs for gtree only.
**/

#ifndef RECLAIMER_NBR_H
#define RECLAIMER_NBR_H

#include "arraylist.h"
#include "hashtable.h"
#include "recovery_manager.h"

class reclaimer_nbr
{
private:
    //algorithm specific macros
    static const int MAX_PER_THREAD_HAZARDPTR = 4; //4 for abtree viol node       //3 for guerraoui delete, and 2 for lazy list.
    static const int MAX_RETIREBAG_CAPACITY_POW2 = 512; //16384;//32768; //16384;

    //local vars and data structures
    unsigned int num_process;
    PAD;
    unsigned int numtimes_sig_sent;
    PAD;

    class ThreadData
    {
    private:
        PAD;

    public:
        // blockbag<node_t> *retiredBag;
        ArrayList *retiredBag;
        PAD;
        AtomicArrayList *proposedHzptrs;
        PAD;
        hashset_new *scannedHzptrs;
        unsigned int bagCapacityThreshold; // using this variable to set random thresholds for out of patience.

        unsigned int num_sigs;

        ThreadData()
        {
        }

    private:
        PAD;
    };

    PAD;
    ThreadData threadData[MAX_THREADS_POW2];
    PAD;

    /**
     * tells a calling thread whether its retire bag has reached to a threshold size (MAX_RETIREBAG_CAPACITY_POW2) when it's time to empty it.
     * Each threads threshold size is set randomly to avoid all threads reaching size threashold at the same time. Note this is an optimization
     * as it would prevent all threads from bottlenecking whole system by sending signals at the same time.
    */
    inline bool isOutOfPatience(const int tid)
    {
        // return (((threadData[tid].retiredBag->getSizeInBlocks() - 1) * BLOCK_SIZE + threadData[tid].retiredBag->getHeadSize()) > threadData[tid].bagCapacityThreshold);
        bool isCapreached = ( (threadData[tid].retiredBag->size() ) == threadData[tid].bagCapacityThreshold );

        // if (isCapreached)
        // {
        //     DEBUG COUTATOMICTID("bagCapacityThreshold=" << threadData[tid].bagCapacityThreshold << " "<< threadData[tid].retiredBag->size()<< std::endl);
        //     // assert (!isCapreached && "isOutOfPatience!!!");
        // }
        return isCapreached;
    }

    /**
     * Whenever a thread's retirebag reaches the threshold size (MAX_RETIREBAG_CAPACITY_POW2) then it sends signals to all other threads in the system
     * using pthread_kill(). Thus invoking NBRsighandler() in recoverymanager.h
    */
    inline bool requestAllThreadsToRestart(const int tid)
    {

        bool result = false;

        for (int otherTid = 0; otherTid < num_process; ++otherTid)
        {
            if (tid != otherTid)
            {
                //get posix thread id from application level thread id.
                pthread_t otherPthread = this->recoveryMgr->getPthread(otherTid);
                int error = 0;
                //send signal to other thread
                // DEBUG COUTATOMICTID("DEBUG_TID_MAP::"
                //                     << " tid=" << tid << " with pid=" << pthread_self() << " registeredThreads[" << tid << "]=" << registeredThreads[tid] << " sending sig to tid= " << otherTid << " with pid=" << otherPthread << " registeredThreads[" << otherTid << "]=" << registeredThreads[otherTid] << std::endl);


                if (error = pthread_kill(otherPthread, this->recoveryMgr->neutralizeSignal))
                {
                    // COUTATOMICTID("Error when trying to pthread_kill(pthread_tFor(" << otherTid << "), " << this->recoveryMgr->neutralizeSignal << ")" << std::endl);
                    // if (error == ESRCH)
                    //     COUTATOMICTID("ESRCH" << std::endl);
                    // if (error == EINVAL)
                    //     COUTATOMICTID("EINVAL" << std::endl);

                    assert("Error when trying to pthread_kill" && 0);
                    return result;
                }
                // else
                // {
                //     DEBUG COUTATOMICTID(" Signal sent via pthread_kill(pthread_tFor(" << otherTid << "  " << otherPthread << "), " << this->recoveryMgr->neutralizeSignal << ")" << std::endl);
                // }
            } // if (tid != otherTid){
        }     // for()

        threadData[tid].num_sigs++;
        result = true;
        return result;
    }

    /**
     * When it's time to empty retirebag a threads first scans all the hazard pointers of all other threads and saves them in a hashtable (scannedHzptrs).
     * This is efficient as now when a thread would free its retired records only O(nk) + O(m) time would be needed,
     * where m=size of retirebag, n=#threads, k=max hazard pointers per thread.
     * Without this optimization O(mnk) time would be needed to free all record in retirebag.
     *
    */
    inline void collectAllSavedRecords(const int tid)
    {

        threadData[tid].scannedHzptrs->clear();
        assert("scannedHzptrs size should be 0 before collection" && threadData[tid].scannedHzptrs->size() == 0);

        for (int otherTid = 0; otherTid < /*this->NUM_PROCESSES*/num_process; ++otherTid)
        {
            if (otherTid != tid){
            //FIXME: Don't know why skipping to collect own HPs caused gtree validation failure?? --> Ans is in nbr Jr notes.
            // COUTATOMICTID("begin sz otid="<<otherTid<<" "<<threadData[otherTid].proposedHzptrs<<std::endl);
            assert(threadData[otherTid].proposedHzptrs && "HP list for this thread is NULL");
            unsigned int sz = threadData[otherTid].proposedHzptrs->size(); //size shouldn't change during execution.
            assert("prposedHzptr[othertid] should be less than max" && sz <= MAX_PER_THREAD_HAZARDPTR);

            //for each hazard pointer in othertid proposed hazard ptrs.
            for (int ixHP = 0; ixHP < sz; ++ixHP)
            {
                node_t *hp = (node_t *)threadData[otherTid].proposedHzptrs->get(ixHP);
                if (hp)
                {
                    threadData[tid].scannedHzptrs->insert((node_t *)hp);
                }
            }
            } //if (otherTid != tid)
        }
    }

    /**
     * Frees all records in retireBag exclusing any records which are hazard pointer protected. We use setbenches pool interface to free records currently
     * adding to pool implies that all records are immediately freed as we use pool_none.
     * sigsafe implies that any thread executing this wouldn't be restarted(neutralized) on receiving signals.
    */
    inline void sendFreeableRecordsToPool(const int tid)
    {
        //get apointer to retirbag of current thread
        ArrayList *const freeable = threadData[tid].retiredBag;

        // DEBUG COUTATOMICTID("TR:: sigSafe: sendFreeableRecordsToPool: Before freeing retiredBag size in nodes=" << threadData[tid].retiredBag->size() << std::endl);

        node_t *ptr;

        // one by one remove records from teh freeable arraylist.

        for (int ix = 0; ix < freeable->size(); ix++)
        {
            ptr = freeable->get(ix);
            if (! threadData[tid].scannedHzptrs->contains(ptr) )
            {
                freeable->erase(ix);
                free(ptr);
            }
            //else skip freeing ptr, it is protected.
        }

        // COUTATOMICTID("After freeing retiredBag size in nodes=" << threadData[tid].retiredBag->size() << std::endl);
    }

    /**
     * two step process to empty the retire bag: 1) collects all records that are not HP protected (freeable records) and then 2) frees them (add to pool).
    */
    inline bool reclaimFreeable(const int tid)
    {
        bool result = false;
        collectAllSavedRecords(tid);
        sendFreeableRecordsToPool(tid);
        return true;
    }

public:
    RecoveryMgr * recoveryMgr;

    /**
     * public api. Data structure operation should save current thread context using NBR_SIGLONGJMP_TARGET before invoking startOP. As if startOp called in reverse order then a thread could siglongjmp to a previous operation's saved context which could lead to undefined or unsafe executions.
    */
    inline bool startOp(const int tid)
    {
        bool result = false;

        assert("restartable value should be 0 in before startOp. Check NBR usage rules." && restartable == 0);
        threadData[tid].proposedHzptrs->clear();
        assert("proposedHzptrs->size should be 0" && threadData[tid].proposedHzptrs->size() == 0);

// #ifdef RELAXED_RESTARTABLE_DELME
//     restartable = 1;
// #elif FAA_RESTARTABLE
//     __sync_fetch_and_add(&restartable, 1);
// #elif FE_RESTARTABLE
//     __sync_lock_test_and_set(&restartable, 1);
// #else
    CASB(&restartable, 0, 1); //assert(CASB (&restartable, 0, 1));
// #endif
    assert("restartable value should be 1" && restartable == 1);
    result = true;
    return result;
    }

    /**
     * Public API. Protect all the discovered pointers in an operation's search phase before entering the write phase. This ensures that when I am in write phase possibly dereferencing these pointers no other thread could reclaim/free these records.
    */
    inline void saveForWritePhase(const int tid, node_t *const record)
    {
        if (!record) return;

        assert("proposedHzptrs ds should be non-null" && threadData[tid].proposedHzptrs);
        assert("record to be added should be non-null" && record);
        assert("HzBag is full. Increase MAX_PER_THREAD_HAZARDPTR" && !threadData[tid].proposedHzptrs->isFull());

        threadData[tid].proposedHzptrs->add(record);
    }

    /**
     * after invoking saveForWritePhase() required number of times use this API to imply that a thread entered in writephase thus non-restartable. Meaning that when this thread receives a neutralizing signal it will execute signal handler (NBRsighandler) and would just return to resume what its doing in writephase.
    */
    inline void upgradeToWritePhase(const int tid)
    {
        // DEBUG COUTATOMICTID("TR:: upgradeToWritePhase: SigSafe" << std::endl);
        assert("restartable value should be 1 before write phase" && restartable == 1);
// #ifdef RELAXED_RESTARTABLE
//         restartable = 0; // writephase instruction wont go in read phase due to proposedHp being atomic
// #elif FAA_RESTARTABLE
//     __sync_fetch_and_add(&restartable, -1);
// #elif FE_RESTARTABLE
//     __sync_lock_test_and_set(&restartable, 0);
// #else
    CASB(&restartable, 1, 0); //assert (CASB (&restartable, 1, 0));
// #endif
        assert("restartable value should be 0 in write phase" && restartable == 0);
    }

    /*
    * Use the API to clear any operation specific ds which won't be required across operations.
    *  i) clear ds populated in saveForWrite
    *  ii) USERWARNING: any record saved for write phase must be released in this API by user. Thus user should call this API at
    *       all control flows that return from dsOP.
    */
    inline void endOp(const int tid)
    {
        assert("proposedHzptrs ds should be non-null" && threadData[tid].proposedHzptrs);

// #ifdef RELAXED_RESTARTABLE_DELME
//         restartable = 0;
// #elif FAA_RESTARTABLE
// if (restartable)
// {
//     __sync_fetch_and_add(&restartable, -1);
// }
// else
// {
//     __sync_fetch_and_add(&restartable, 0);
// }
// #elif FE_RESTARTABLE
//     __sync_lock_test_and_set(&restartable, 0);
// #else
        CASB(&restartable, 1, 0);
// #endif

        assert("restartable value should be 0 in post endOP" && restartable == 0);
    }

    /*
    * Tells whether the reclaimer uses signalling. Use this API to distinguish a NBR specific call using record manager inside a ds operation. You may use this API to tell that the function is meant to invoke NBR's API and skip unnecessarily invoking NBR specific API while using Debra or other reclaimers.
    */
    inline static bool needsSetJmp()
    {
        return true;
    }

    /**
     * Public API. A ds calls this API after logically deleting (unlinking) a record from DS. It saves the record in retireBag for a delayed free.
    */
    inline void retire(const int tid, node_t *record)
    {
        if (isOutOfPatience(tid))
        {
            // DEBUG COUTATOMICTID("TR:: outOfPatience: SigSafe: retiredBag BlockSize=" << threadData[tid].retiredBag->size() << "retiredBag Size nodes=" << threadData[tid].retiredBag->size() << std::endl);
            if (requestAllThreadsToRestart(tid))
            {
                // DEBUG COUTATOMICTID("TR:: sigSafe: outOfPatience: restarted all threads, gonna continue reclaim =" << threadData[tid].retiredBag->size() << " block" << std::endl);
                reclaimFreeable(tid);
            }
            else
            {
                // COUTATOMICTID("TR:: retire: Couldn't restart all threads!" << std::endl);
                assert("Couldn't restart all threads continuing execution could be unsafe ..." && 0);
                exit(-1);
            }
        } // if (isOutOfPatience(tid)){

        assert(!threadData[tid].retiredBag->isFull());
        assert("retiredBag ds should be non-null" && threadData[tid].retiredBag);
        assert("record to be added should be non-null" && record);
        threadData[tid].retiredBag->add(record);
    }

    // void debugPrintStatus(const int tid)
    // {
    //     DEBUG COUTATOMICTID("debugPrintStatus: retiredBag Size=" << threadData[tid].retiredBag->size() << std::endl);
    // }

    /**
     * API that supports harness. Definition is just debug messages. recordmanager, data structure's and record_manager_single_type initThread does the work.
     * Make sure call init deinit pair in setbench harness (main.cpp) to ensure that recovery.h's reseervedThread[] contains correct pthread ids to avoid signalling wrong thread.
     */
    void initThread(const int tid)
    {

        // COUTATOMICTID("nbr: initThread\n");
        threadData[tid].bagCapacityThreshold = MAX_RETIREBAG_CAPACITY_POW2;

        threadData[tid].retiredBag = new ArrayList(MAX_RETIREBAG_CAPACITY_POW2);
        threadData[tid].proposedHzptrs = new AtomicArrayList(MAX_PER_THREAD_HAZARDPTR);
        threadData[tid].scannedHzptrs = new hashset_new(num_process * MAX_PER_THREAD_HAZARDPTR);
        threadData[tid].num_sigs = 0;

        // DEBUG COUTATOMICTID("NBR "<< tid <<" - " << pthread_self() << std::endl);
        assert (recoveryMgr);
        recoveryMgr->initThread(tid);
    }

    void deinitThread(const int tid)
    {
        // COUTATOMICTID("nbr: deinitThread\n");

        numtimes_sig_sent += threadData[tid].num_sigs;

        for (int ix = 0; ix < threadData[tid].retiredBag->size(); ix++)
        {
            node_t *ptr = threadData[tid].retiredBag->get(ix);
            threadData[tid].retiredBag->erase(ix);
            free(ptr);
        }

        delete threadData[tid].retiredBag;
        delete threadData[tid].proposedHzptrs;
        delete threadData[tid].scannedHzptrs;

        threadData[tid].retiredBag = NULL;
        threadData[tid].proposedHzptrs = NULL;
        threadData[tid].scannedHzptrs = NULL;

        recoveryMgr->deinitThread(tid);
    }

    reclaimer_nbr(const int numProcess /*, RecoveryMgr<void *> *const _recoveryMgr = NULL*/)
    {
        num_process = numProcess;
        // DEBUG COUTATOMIC("gonna new Recovery Mgr numProcess="<<numProcess<<std::endl);
        // std::cout<< std::flush;
        recoveryMgr = new RecoveryMgr(numProcess, SIGQUIT);

        // COUTATOMIC("MAX_RETIREBAG_CAPACITY_POW2 =" <<MAX_RETIREBAG_CAPACITY_POW2 <<std::endl);
        // if (_recoveryMgr)
        //     COUTATOMIC("SIGRTMIN=" << SIGRTMIN << " neutralizeSignal=" << this->recoveryMgr->neutralizeSignal << std::endl);

        // if (MAX_RETIREBAG_CAPACITY_POW2 == 0)
        // {
        //     COUTATOMIC("give a valid value for MAX_RETIREBAG_CAPACITY_POW2!" << std::endl);
        //     exit(-1);
        // }

        numtimes_sig_sent = 0;
    }

    ~reclaimer_nbr()
    {
        // VERBOSE DEBUG COUTATOMIC("destructor reclaimer_nbr" << std::endl);
        COUTATOMIC("numtimes_signals_sent=" <<numtimes_sig_sent <<std::endl);
        delete recoveryMgr;
    }
};

#endif /* RECLAIMER_NBR_H */
