#include <stdio.h>
#include <time.h>
#include <string.h>
#include <thread>
#include "ticket_impl.h"

#define MAX_THREADS 512
#define MIN_PREFILL_THRESHOLD_FRACTION 0.95

#define RESERVED_VALUE (-1)

class InitSplitMixer {
private:
    uint64_t s;
public:
    InitSplitMixer(uint64_t seed) {
        s = seed;
    }
    uint64_t next() {
        uint64_t result = (s += 0x9E3779B97f4A7C15);
        result = (result ^ (result >> 30)) * 0xBF58476D1CE4E5B9;
        result = (result ^ (result >> 27)) * 0x94D049BB133111EB;
        return result ^ (result >> 31);
    }
};

// xoshiro256+ generator
// WARNING: only generates random numbers up to 2^53-1 for the sake of quality...
class Random53 {
private:
    #define NUM_64BIT_WORDS 4
    union {
        volatile int64_t padding[192];
        uint64_t s[NUM_64BIT_WORDS];
    };
    uint64_t rol64(uint64_t x, int k) {
        return (x << k) | (x >> (64 - k));
    }
public:
    void setSeed(uint64_t seed) {
        InitSplitMixer subseedGenerator (seed);
        for (int i=0;i<NUM_64BIT_WORDS;++i) {
            s[i] = subseedGenerator.next();
        }
    }
    Random53() {}
    Random53(uint64_t seed) {
        setSeed(seed);
    }
    uint64_t next() {
        uint64_t const result = s[0] + s[3];
        uint64_t const t = s[1] << 17;
        s[2] ^= s[0];
        s[3] ^= s[1];
        s[1] ^= s[2];
        s[0] ^= s[3];

        s[2] ^= t;
        s[3] = rol64(s[3], 45);
        // discard low order bits to preserve only high quality top 53 bits
        return result >> 11;
    }
    uint64_t next(uint64_t n) {
        return next() % n;
    }
};

class ElapsedTimer {
private:
    char padding0[192];
    bool calledStart = false;
    char padding1[192];
    struct timespec startTime;
    char padding2[192];
public:
    void start() {
        calledStart = true;
        clock_gettime(CLOCK_MONOTONIC, &startTime);
    }
    int64_t getElapsedMillis() {
        if (!calledStart) {
            printf("ERROR: called getElapsedMillis without calling startTimer\n");
            exit(1);
        }
        struct timespec now;
        clock_gettime(CLOCK_MONOTONIC, &now);
        long nowmillis = now.tv_sec*1000 + now.tv_nsec/1000000;
        long startmillis = startTime.tv_sec*1000 + startTime.tv_nsec/1000000;
        return nowmillis - startmillis;
    }
};

class Barrier {
private:
    char padding0[192];
    const int releaseValue;
    char padding1[192];
    volatile int v;
    char padding2[192];
public:
    Barrier(int _releaseValue) : releaseValue(_releaseValue), v(0) {}
    void wait() {
        __sync_fetch_and_add(&v, 1);
        while (v < releaseValue) {}
    }
};

class pint64_t {
public:
    union {
        char padding[192];
        volatile int64_t value;
    };
};

class globals_t {
public:
    char padding0[192];
    Barrier * barrierPrefill;
    Barrier * barrierWork;
    ElapsedTimer * timerPrefill;
    ElapsedTimer * timerWork;
    int64_t numThreads;
    int64_t millisToRun;
    int64_t maxkey;
    double ins;
    double del;
    char padding1[192];
    ticket_impl * dataStructure;
    char padding2[192];
    volatile int64_t operationsPerformed;
    char padding3[192];
    volatile pint64_t sizeChecksumPerThread[MAX_THREADS];
    char padding4[192];

    globals_t() : barrierPrefill(NULL), barrierWork(NULL), timerPrefill(NULL), timerWork(NULL)
            , numThreads(8), millisToRun(3000), maxkey(100000), ins(5), del(5)
            , dataStructure(0), operationsPerformed(0) {}
    ~globals_t() {
        if (barrierPrefill) delete barrierPrefill;
        if (barrierWork) delete barrierWork;
        if (timerPrefill) delete timerPrefill;
        if (timerWork) delete timerWork;
        if (dataStructure) delete dataStructure;
    }
};
globals_t * g;

int64_t getExpectedSize() {
    return g->maxkey * g->ins / (g->ins + g->del);
}

int64_t isSizeWithinTolerance(int size) {
    return size >= MIN_PREFILL_THRESHOLD_FRACTION*getExpectedSize();
}



void threadPrefill(int tid, Random53 * rng) {

    // helps to init recovery manager internal arrays.
    g->dataStructure->initThread(tid);

    // wait for all threads to start
    g->barrierPrefill->wait();

    // do operations
    int64_t i;
    int64_t mySizeChecksum = 0;
    for (i=0; ; ++i) {
        // periodically check the time to see if we should stop prefilling
        if ((i & ((1<<12)-1)) == 0) {
            g->sizeChecksumPerThread[tid].value = mySizeChecksum;
            int64_t totalSizeChecksum = 0;
            for (int otherTid=0;otherTid<g->numThreads;++otherTid) {
                totalSizeChecksum += g->sizeChecksumPerThread[otherTid].value;
            }
            if (isSizeWithinTolerance(totalSizeChecksum)) break;
            if (tid == 0) printf("current total size checksum of %ld is not within tolerance yet\n", totalSizeChecksum);
        }

        uint64_t randKey = 1+(rng->next() % g->maxkey);
        double randPercent = (rng->next() % 1000000000) / 999999999.0;

        if (randPercent < (g->ins / (g->ins + g->del))) {
            uint64_t oldval = g->dataStructure->bst_tk_insert(tid, randKey, randKey);
            bool insSuccess = (oldval == RESERVED_VALUE);
            if (insSuccess) mySizeChecksum++;
        } else {
            uint64_t oldval = g->dataStructure->bst_tk_delete(tid, randKey);
            bool delSuccess = (oldval != RESERVED_VALUE);
            if (delSuccess) mySizeChecksum--;
        }
    }
    // __sync_fetch_and_add(&g->operationsPerformed, i+1); // don't count for prefilling
}

void threadWork(int tid, Random53 * rng) {
    // wait for all threads to start
    g->barrierWork->wait();

    // do operations
    int64_t i;
    for (i=0; ; ++i) {
        // periodically check the time to see if we should stop doing operations
        // note: error in throughput is proportional to nthreads * number of ops between time checks divided by time.
        if ((i & ((1<<6)-1)) == 0) if (g->timerWork->getElapsedMillis() >= g->millisToRun) break;

        uint64_t randKey = 1+(rng->next() % g->maxkey);
        double randPercent = (rng->next() % 1000000000) / 999999999.0;

        if (randPercent < g->ins) {
            uint64_t oldval = g->dataStructure->bst_tk_insert(tid, randKey, randKey);
        } else if (randPercent < g->ins + g->del) {
            uint64_t oldval = g->dataStructure->bst_tk_delete(tid, randKey);
        } else {
            uint64_t val = g->dataStructure->bst_tk_find(tid, randKey);
        }
    }
    __sync_fetch_and_add(&g->operationsPerformed, i+1);

    //deinit metadata that is tid dependent
    g->dataStructure->deinitThread(tid);
}

void threadFunc(int tid, Random53 * rng) {
    threadPrefill(tid, rng);
    threadWork(tid, rng);
}

void runExperiment() {
    g->dataStructure = new ticket_impl(/*UNUSED nthreads=*/g->numThreads, /*key_min=*/ -1, /*key_max=*/ g->maxkey+1000, /*val_reserved=*/ RESERVED_VALUE, /*UNUSED index=*/ -1);

    printf("starting threads...\n");
    srand(time(0));
    std::thread * threads[MAX_THREADS];
    Random53 rngs[MAX_THREADS];
    for (int tid=0;tid<g->numThreads;++tid) {
        rngs[tid].setSeed(rand());
        threads[tid] = new std::thread(threadFunc, tid, &rngs[tid]);
    }

    g->timerPrefill->start();
    g->barrierPrefill->wait();
    printf("threads prefilling...\n");

    g->timerWork->start();
    g->barrierWork->wait();
    printf("threads working...\n");

    for (int tid=0;tid<g->numThreads;++tid) {
        threads[tid]->join();
        delete threads[tid];
    }
    // printf("operations=%ld\n", g->operationsPerformed);
    printf("throughput=%ld\n", g->operationsPerformed * 1000 / g->millisToRun);
}

int main(int argc, char ** argv) {
    g = new globals_t();

    if (argc != 11) {
        printf("USAGE: %s -i INSERT_PERCENT -d DELETE_PERCENT -k MAX_KEY -n THREADS -t MILLIS_TO_RUN\n", argv[0]);
        exit(1);
    }

    for (int i=1;i<argc;++i) {
        if (strcmp(argv[i], "-i") == 0) {
            g->ins = atof(argv[++i]);
        } else if (strcmp(argv[i], "-d") == 0) {
            g->del = atof(argv[++i]);
        } else if (strcmp(argv[i], "-k") == 0) {
            g->maxkey = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-n") == 0) {
            g->numThreads = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-t") == 0) {
            g->millisToRun = atoi(argv[++i]);
        // } else if (strcmp(argv[i], "-pin") == 0) { // e.g., "-pin 1,2,3,8-11,4-7,0"
        //     binding_parseCustom(argv[++i]);        // e.g., "1,2,3,8-11,4-7,0"
        //     printf("parsed custom binding: %s\n", argv[i]);
        } else {
            printf("bad argument %s\n", argv[i]);
            exit(1);
        }
    }

    g->barrierPrefill = new Barrier(1+g->numThreads);
    g->barrierWork = new Barrier(1+g->numThreads);
    g->timerPrefill = new ElapsedTimer();
    g->timerWork = new ElapsedTimer();

    const int numThreads = atoll(argv[1]);
    const int millisToRun = atoll(argv[2]);

    runExperiment();
    delete g;
    return 0;
}
